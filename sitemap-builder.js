const fs = require("fs");
const path = require("path");
const sm = require("sitemap");
const sitemapLocation = "public/sitemap.xml";

const sitemap = sm.createSitemap({
  hostname: "https://carecards.io",
  cacheTime: 600000,
  urls: [
    { url: "/", changefreq: "weekly", priority: 1 },
    { url: "/cards", changefreq: "weekly", priority: 0.9 },
    { url: "/info", changefreq: "weekly", priority: 0.8 }
  ]
});

function writeFile(location, data) {
  fs.writeFile(location, data, err => {
    if (err) throw err;
    console.log(`Sitemap saved at ${sitemapLocation}!`);
  });
}

writeFile(path.resolve(sitemapLocation), sitemap.toString());
