# Care Cards

## Features

- [x] React Router
- [x] Prettier & ESLint
- [x] SVG sprites
- [x] Styled Components

## Getting started

Below is a list of commands you will probably find useful.

## Getting started

- Install: `npm install`
- Start dev: `npm start`
- Production build: `npm run build`
- Build sitemap: `npm run sitemap`
- Deploy: `npm run deploy`
