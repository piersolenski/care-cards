// Dependencies
import React from "react";
import styled from "styled-components";

const Wrapper = styled.figure`
  position: relative;
  margin: 140px 0 0 0;
  @media (max-width: 767px) {
    display: none;
  }
  @media (min-width: 768px) {
    grid-column: 7 / 13;
  }
  @media (min-width: 1200px) {
    grid-column: 8 / 13;
  }
`;

const Video = styled.video`
  position: absolute;
  top: 8.452250274%;
  left: 15.166340509%;
  width: 68.395303327%;
`;

const Phone = () => (
  <Wrapper>
    <img src={require("../images/phone.png")} alt="Phone" />
    <Video autoPlay playsInline muted loop src={require("../video/demo.mp4")} poster={require("../images/video-poster.png")}/>
  </Wrapper>
);

export default Phone;
