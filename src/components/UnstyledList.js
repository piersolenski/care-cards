// Dependencies
import styled from "styled-components";

const UnstyledList = styled.ul`
  list-style: none;
  padding-left: 0;
`;

export default UnstyledList;
