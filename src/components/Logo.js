// Dependencies
import React from "react";
import styled from "styled-components";
import Icon from "../components/Icon";
// Config
import COLORS from "../config/colors";

const Wrapper = styled.h1`
  display: flex;
  align-items: center;
  font-size: 18px;
  grid-column: 1 / 4;
`;

const Text = styled.span`
  font-family: "Walsheim";
  color: ${COLORS.white};
`;

const LogoIcon = styled(Icon)`
  margin-right: 10px;
  width: 47px;
  height: 44px;
`;

const Logo = () => (
  <Wrapper>
    <LogoIcon glyph={require("../images/sprite/logo.svg")} />
    <Text>Care Cards</Text>
  </Wrapper>
);

export default Logo;
