// Packages
import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch
} from "react-router-dom";
// Containers
import Intro from "./containers/Intro";
import Cards from "./containers/Cards";
import Info from "./containers/Info";
// Utils
import trackPage from "./utils/trackPage";

const InnerApp = () => (
  <Switch>
    <Route exact path="/" component={Intro} />
    <Route exact path="/cards" component={Cards} />
    <Route exact path="/info" component={Info} />
    <Redirect from="*" to="/" />
  </Switch>
);

const App = () => (
  <Router>
    <Route component={trackPage(InnerApp)} />
  </Router>
);

export default App;
