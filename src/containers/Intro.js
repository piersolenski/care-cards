// Packages
import React, { Component } from "react";
import styled from "styled-components";
// Config
import COLORS from "../config/colors";
// Components
import Button from "../components/Button";
import Logo from "../components/Logo";
import Icon from "../components/Icon";
import Phone from "../components/Phone";
import UnstyledList from "../components/UnstyledList";
import { Row, RowInner } from "../components/Layout";
import FloatingMan from "../components/FloatingMan";
// Utils
import between from "../utils/between";

const FloatingManIllustration = styled(FloatingMan)`
  width: 180px;
  margin-bottom: 16px;
`;

const Viewport = styled.div`
  @media (max-width: 767px) {
    height: 100%;
  }
  @media (min-width: 768px) {
    min-height: 100%;
    position: relative;
    overflow: hidden;
    &:before,
    &:after {
      display: block;
      content: "";
      position: absolute;
      width: 100%;
      height: 800px;
      top: 0;
      left: 0;
    }
    &:before {
      border-bottom-right-radius: 100px;
      background: ${COLORS.blue};
      transform: rotate(6deg);
      transform-origin: top right;
    }
    &:after {
      width: 120%;
      background: ${COLORS.blue};
      opacity: 0.1;
      z-index: -5;
      transform: rotate(-6deg) translateX(-5%);
      transform-origin: bottom left;
    }
  }
`;

const Header = Row.extend`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  padding: 40px 0;
  @media (max-width: 767px) {
    display: none;
  }
`;

const Hero = Row.extend`
  color: ${COLORS.white};
  @media (max-width: 767px) {
    background: ${COLORS.blue};
    height: 100%;
  }
`;

const HeroInner = RowInner.extend`
  position: relative;
`;

const Main = Row.extend`
  color: #828499;
  margin-bottom: 100px;
  @media (max-width: 767px) {
    display: none;
  }
  @media (max-width: 900px) {
    margin-top: 12vh;
  }
`;

const Footer = Row.extend`
  background: #f2f6fd;
  color: #828499;
  padding: 80px 0;
  @media (max-width: 767px) {
    display: none;
  }
`;

const Glyph = styled(Icon)`
  height: 20px;
  display: block;
  margin-bottom: 16px;
`;

const H1 = styled.h1`
  ${between("font-size", 320, 1000, 25, 38)};
  line-height: 1.2em;
  margin-top: 0;
  margin-bottom: 10px;
`;

const H2 = styled.h2`
  font-size: 22px;
  line-height: 1.2em;
  margin-top: 0;
  margin-bottom: 10px;
  span {
    color: ${COLORS.blue};
  }
`;

const P = styled.p`
  line-height: 1.6em;
  max-width: 400px;
  @media (min-width: 1200px) {
    font-size: 18px;
  }
`;

const Introduction = styled.div`
  @media (max-width: 767px) {
    grid-column: 1 / -1;
    align-self: center;
  }
  @media (min-width: 768px) {
    margin-top: 200px;
    grid-column: 1 / 6;
  }
  @media (min-width: 1200px) {
    grid-column: 2 / 6;
  }
`;

const Description = styled.div`
  @media (max-width: 1099px) {
    display: none;
  }
  @media (min-width: 1100px) {
    grid-column: 1 / 6;
  }
  @media (min-width: 1200px) {
    grid-column: 2 / 6;
  }
`;

const List = styled(UnstyledList)`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 1.5em;
  grid-column: 1 / -1;
  font-size: 15px;
  @media (min-width: 1100px) {
    grid-column: 7 / 13;
  }
`;

const AbstractCards = styled(Icon)`
  width: 100%;
  @media (max-width: 1199px) {
    grid-column: 1 / 3;
    position: absolute;
  }
  @media (min-width: 1200px) {
    grid-column: 2 / 4;
  }
`;

const A = styled.a`
  color: ${COLORS.turquoise};
  white-space: nowrap;
`;

const Credits = styled.div`
  font-size: 18px;
  grid-column: 4 / 10;
  @media (min-width: 1000px) {
    grid-column: 5 / 9;
  }
`;

const Contact = styled.div`
  font-size: 15px;
  grid-column: 4 / -1;
  @media (min-width: 1000px) {
    grid-column: 11 / 13;
  }
`;

const FooterInner = RowInner.extend`
  align-items: center;
  position: relative;
`;

class Intro extends Component {
  // componentDidMount() {
  //   if (localStorage.introRead) this.props.history.push("/cards");
  // }
  setAsRead() {
    // if (process.env.NODE_ENV === "production") {
    //   localStorage.setItem("introRead", true);
    // }
  }
  render() {
    return (
      <Viewport>
        <Header>
          <RowInner>
            <Logo />
          </RowInner>
        </Header>
        <Hero>
          <HeroInner>
            <Introduction>
              <FloatingManIllustration speed={1.5} />
              <H1>
                A set of tips to <br /> keep you afloat
              </H1>
              <P>
                Care Cards are a set of kind thoughts and helpful little
                activities to keep you on top of your mental well-being.
              </P>
              <Button
                to="/cards"
                color="pink"
                onClick={this.setAsRead.bind(this)}
              >
                Let's go
              </Button>
            </Introduction>
            <Phone />
          </HeroInner>
        </Hero>
        <Main>
          <RowInner>
            <Description>
              <H2>
                Care Cards are <span>your little helpers</span> when you need
                them
              </H2>
              <P>
                We can all get overwhelmed from time to time. Care Cards are
                your reminder to take care of yourself, one little tip at a
                time.
              </P>
            </Description>
            <List>
              <li>
                <Glyph glyph={require("../images/sprite/sunrise.svg")} />
                Over 80 tips (and counting) to brighten up your day
              </li>
              <li>
                <Glyph glyph={require("../images/sprite/hourglass.svg")} />
                All our tips are quick and easily achievable. Rome wasn't built
                in a day!
              </li>
              <li>
                <Glyph glyph={require("../images/sprite/palette.svg")} />A
                beautifully designed experience to set your mind at ease
              </li>
            </List>
          </RowInner>
        </Main>
        <Footer>
          <FooterInner>
            <AbstractCards
              glyph={require("../images/sprite/abstract-cards.svg")}
            />
            <Credits>
              Care Cards is a project initiated by the digital collective{" "}
              <A href="https://firstandforemost.co/">First and Foremost</A>.
            </Credits>
            <Contact>
              Have a suggestion to add to our cards?{" "}
              <A href="mailto:contact@firstandforemost.co">Drop us a line.</A>
            </Contact>
          </FooterInner>
        </Footer>
      </Viewport>
    );
  }
}

export default Intro;
