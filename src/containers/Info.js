// Packages
import React, { Component } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
// Components
import Icon from "../components/Icon";
// Config
import COLORS from "../config/colors";
// Utils
import { fade } from "../utils/keyframes";
// Data
import charities from "../data/charities";

const Viewport = styled.div`
  display: grid;
  color: ${COLORS.purpleDark};
  @media (min-width: 1000px) {
    grid-template-columns: 38% 62%;
    grid-template-rows: 1fr auto;
    min-height: 100vh;
  }
`;

const Header = styled.header`
  color: ${COLORS.white};
  background: ${COLORS.turquoise};
  padding: 7.5vw;
  @media (max-width: 999px) {
    text-align: center;
  }
  @media (min-width: 1000px) {
    padding: 4vw;
    grid-column: 1;
  }
  > * {
    animation: ${fade} 0.75s ease;
  }
`;

const Main = styled.main`
  background: ${COLORS.greyLight};
  padding: 7.5vw;
  @media (min-width: 1000px) {
    padding: 4vw;
    grid-column: 2;
  }
  > * {
    animation: ${fade} 0.75s ease;
  }
`;

const Footer = styled.footer`
  color: ${COLORS.purpleLight};
  background: ${COLORS.purpleDark};
  padding: 7.5vw;
  @media (min-width: 1000px) {
    padding: 4vw;
    grid-column: 1 / 3;
  }
  @media (max-width: 1199px) {
    text-align: center;
  }
  @media (min-width: 1200px) {
    display: flex;
    justify-content: space-between;
  }
  > * {
    animation: ${fade} 0.75s ease;
  }
`;

const H1 = styled.h1`
  font-size: 25px;
  line-height: 30px;
  margin-top: 0;
  margin-bottom: 10px;
`;

const H2 = styled.h1`
  font-size: 20px;
  line-height: 20px;
  margin-top: 0;
  margin-bottom: 10px;
`;

const UnstyledList = styled.ul`
  list-style: none;
  padding-left: 0;
  display: grid;
  grid-gap: 20px;
`;

const Li = styled.li`
  &:not(:last-child) {
    padding-bottom: 20px;
    border-bottom: 1px solid ${COLORS.greyMedium};
  }
`;

const A = styled.a`
  color: ${COLORS.turquoise};
  display: ${props => props.block && "block"};
  svg {
    height: 16px;
    margin-right: 8px;
    color: ${COLORS.purpleDark};
    vertical-align: -2px;
  }
`;

const Smallprint = styled.p`
  margin: 0;
`;

const SmallprintBreak = styled.br`
  @media (min-width: 470px) {
    display: none;
  }
`;

const Logo = styled(Icon)`
  margin-bottom: 20px;
  width: 80px;
`;

const P = styled.p`
  margin-top: 0;
  margin-bottom: 10px;
  &:last-child {
    margin-bottom: 0;
  }
`;

const Close = styled(Link)`
  position: absolute;
  top: 5%;
  right: 10%;
  @media (max-width: 999px) {
    color: ${COLORS.white};
  }
  @media (min-width: 1000px) {
    color: ${COLORS.purpleDark};
    right: 5%;
  }
`;

const CloseIcon = styled(Icon)`
  height: 20px;
`;

class Info extends Component {
  componentDidMount() {
    // fetch("http://ip-api.com/json")
    //   .then(res => res.json())
    //   .then(res => console.log(res.countryCode));
  }
  setAsUnread() {
    // if (process.env.NODE_ENV === "production") {
    //   localStorage.removeItem("introRead");
    // }
  }
  render() {
    return (
      <Viewport>
        <Close to="/cards">
          <CloseIcon glyph={require("../images/sprite/close.svg")} />
        </Close>
        <Header>
          <Link to="/" onClick={this.setAsUnread.bind(this)}>
            <Logo glyph={require("../images/sprite/logo.svg")} />
          </Link>
          <H1>Care Cards is your little helper when you need it</H1>
          <P>
            We all get a little overwhelmed from time to time. Care Cards is a
            set of helpful little tips to keep you on top of your mental
            welbeing. Here are some other resouces you might find helpful.
          </P>
        </Header>
        <Main>
          <UnstyledList>
            {charities.map((charity, i) => (
              <Li key={i}>
                <H2>{charity.name}</H2>
                <P>{charity.description}</P>
                {charity.phone && (
                  <A block href={`tel:${charity.phone}`}>
                    <Icon glyph={require("../images/sprite/phone.svg")} />
                    {charity.phone}
                  </A>
                )}
                <A block href={charity.url} target="_blank">
                  <Icon glyph={require("../images/sprite/link.svg")} />
                  Visit website
                </A>
              </Li>
            ))}
          </UnstyledList>
        </Main>
        <Footer>
          <Smallprint>
            A <A href="https://firstandforemost.co/">First and Foremost</A>{" "}
            project. ✌🏼
          </Smallprint>
          <Smallprint>
            Have a suggestion to add to our cards? <SmallprintBreak />
            <A href="mailto:contact@firstandforemost.co">Drop us a line.</A>
          </Smallprint>
        </Footer>
      </Viewport>
    );
  }
}

export default Info;
